# Android-Java-Kotlin-Jacoco

Sample for jacoco test coverage in an android project with Java and Kotlin files.

Author by [logerom](https://github.com/logerom/android-java-kotlin-jacoco)

I just use the program test CI/CD for Unit test.

## Getting started

1. Build your project
2. run gradle task "testDebugUnitTest"
3. run jacoco gradle task "jacocoTestReport" (created in jacoco.gradle)
4. see report in "app/build/reports/rep/jacocoTestReport"

## Update
* add Robolectric- and Espresso-Tests to jacoco coverage